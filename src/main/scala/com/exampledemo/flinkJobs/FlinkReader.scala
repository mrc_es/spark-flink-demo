package com.exampledemo.flinkJobs

import com.typesafe.config.Config
import org.apache.flink.streaming.api.datastream.DataStream
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment


object FlinkReader {
  def apply(config: Config)
    (implicit env: StreamExecutionEnvironment): Unit = {
    /*1. Lectura */
    val text: DataStream[String] = env.readTextFile(config.getString("app.sourceDir"))

    /*2. Escritura en consola */
    text.print()
    env.execute()
  }
}
