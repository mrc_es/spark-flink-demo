package com.exampledemo

import com.exampledemo.flinkJobs.FlinkReader
import com.exampledemo.sparkJobs.SparkStreamingReader
import org.apache.spark.sql.SparkSession
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.spark.sql

class DemoJob {

  /*[START] Configuracion de Spark*/
  implicit val spark: SparkSession = SparkSession
    .builder
    .appName("SparkStreamingDemo")
    .master("local[2]")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")
  /*[END] Configuracion de Spark*/

  /*[START]Configuracion de Flink*/
  implicit val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment()
  /*[END]Configuracion de Flink*/

  def apply(): Unit = {
    val config: Config = ConfigFactory.load("application.conf")

    //SparkStreamingReader.apply(config)
    FlinkReader.apply(config)
  }
}

object DemoJob {
  def main(args: Array[String]): Unit = {

    val demoJob = new DemoJob
    demoJob()
  }
}
