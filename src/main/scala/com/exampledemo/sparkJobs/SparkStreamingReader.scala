package com.exampledemo.sparkJobs

import com.typesafe.config.Config
import org.apache.spark.sql.SparkSession


object SparkStreamingReader {
  def apply(config: Config)(implicit spark:SparkSession): Unit = {
    /*1. Lectura */
    val lines = spark.readStream
      .text(config.getString("app.sourceDir"))

    /*2. Escritura en consola */
    lines.writeStream
      .outputMode("append")
      .format("console")
      .start()
  }
}
